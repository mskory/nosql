package com.mskory.dao;

import com.datastax.oss.driver.api.core.CqlSession;
import com.datastax.oss.driver.api.core.CqlSessionBuilder;
import com.datastax.oss.driver.api.core.config.DefaultDriverOption;
import com.datastax.oss.driver.api.core.config.DriverConfigLoader;
import com.datastax.oss.driver.api.core.cql.*;
import com.datastax.oss.driver.api.core.servererrors.InvalidQueryException;
import com.datastax.oss.driver.api.core.type.DataType;
import com.datastax.oss.driver.api.core.type.DataTypes;
import com.datastax.oss.driver.api.querybuilder.QueryBuilder;
import com.datastax.oss.driver.api.querybuilder.SchemaBuilder;
import com.datastax.oss.driver.api.querybuilder.insert.RegularInsert;
import com.datastax.oss.driver.api.querybuilder.schema.CreateKeyspace;
import com.datastax.oss.driver.api.querybuilder.schema.CreateTable;
import com.datastax.oss.driver.api.querybuilder.schema.Drop;
import com.mskory.models.Pojo;

import java.lang.reflect.Field;
import java.net.InetSocketAddress;
import java.time.Duration;
import java.util.*;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

import static com.datastax.oss.driver.api.querybuilder.QueryBuilder.bindMarker;
import static com.datastax.oss.driver.api.querybuilder.QueryBuilder.literal;


public class CassandraDB implements DB {
    private static final Map<String, DataType> DATA_TYPES = Map.of(
            "UUID", DataTypes.UUID,
            "int", DataTypes.INT,
            "String", DataTypes.TEXT);
    String dbName;
    int batchSize;
    CqlSession session;

    public CassandraDB() {
        batchSize = config.getBatchSize();
        CqlSessionBuilder builder = CqlSession.builder()
                .addContactPoint(new InetSocketAddress(config.getHostname(), config.getCassandraPort()))
                .addContactPoint(new InetSocketAddress(config.getHostname(), config.getCassandraPort()))
                .withLocalDatacenter(config.getDatacenter())
                .withConfigLoader(DriverConfigLoader.programmaticBuilder()
                        .withDuration(DefaultDriverOption.REQUEST_TIMEOUT, Duration.ofMillis(15000))
                        .withBoolean(DefaultDriverOption.METADATA_SCHEMA_ENABLED, true).build());
        session = builder.build();
        logger.info("Successfully connected to CassandraDB!");
    }

    @Override
    public <T extends Pojo> void insertMany(String tableName, Class<T> clazz, List<T> dto) {
        List<Field> fields = getFields(clazz);
        if (!isTableExists(tableName)) {
            createTable(tableName, fields);
        }
        PreparedStatement preparedStatement = getPrepareStatement(tableName, fields);
        BoundStatement boundStatement = preparedStatement.bind();
        List<BoundStatement> statements = new ArrayList<>(batchSize);
        BatchStatement batch = BatchStatement.newInstance(BatchType.UNLOGGED);
        stopWatch.restart();
        for (T pojo : dto) {
            for (int i = 0; i < fields.size(); i++) {
                Field field = fields.get(i);
                field.setAccessible(true);
                boundStatement = setValueToBoundStatement(i, pojo, field, boundStatement);
                field.setAccessible(false);
            }
            if (statements.size() == batchSize) {
                session.execute(batch.addAll(statements).setKeyspace(dbName));
                statements.clear();
                batch.clear();
            }
            statements.add(boundStatement);
//            session.execute(boundStatement);
        }
        session.execute(batch.addAll(statements).setKeyspace(dbName));
        logger.info("{} records were added to {} in {} seconds", dto.size(), tableName, stopWatch.getTime());
        logger.info("RPS is {}", stopWatch.getRPS(dto.size()));
    }

    private boolean isTableExists(String tableName) {
        String query = "SELECT * FROM system_schema.tables WHERE table_name = ? ALLOW FILTERING";
        PreparedStatement preparedStatement = session.prepare(query);
        BoundStatement boundStatement = preparedStatement.bind(tableName);
        ResultSet resultSet = session.execute(boundStatement);
        return resultSet.one() != null;
    }

    @Override
    public void createDB(String dbName) {
        this.dbName = dbName;
        CreateKeyspace createKeyspace = SchemaBuilder.createKeyspace(dbName)
                .ifNotExists().withSimpleStrategy(15);
        session.execute(createKeyspace.build());
        logger.info("{} keyspace was created", dbName);
    }

    public void createTable(String tableName, List<Field> fields) {
        CreateTable[] createTable = new CreateTable[1];
        createTable[0] = tableName.equals("stock") ?
                SchemaBuilder.createTable(tableName).ifNotExists()
                        .withPartitionKey("categoryId", DataTypes.UUID)
                        .withClusteringColumn("id", DataTypes.UUID) :
                SchemaBuilder.createTable(tableName).ifNotExists()
                        .withPartitionKey("id", DataTypes.UUID);
        fields.stream()
                .map(f -> Map.entry(f.getName(), getType(f.getType())))
                .forEach(e -> createTable[0] = createTable[0].withColumn(e.getKey(), e.getValue()));
        session.execute(createTable[0].build().setKeyspace(dbName));
        logger.info("{} table was created with fields {}", tableName, fields.stream()
                .map(Field::getName)
                .collect(Collectors.toList()));
    }

    private DataType getType(Class<?> clazz) {
        DataType dataType = DATA_TYPES.get(clazz.getSimpleName());
        if (dataType == null) {
            throw new RuntimeException("No such type for " + clazz.getName());
        }
        return dataType;
    }

    private List<Field> getFields(Class<?> clazz) {
        List<Field> fields = new ArrayList<>();
        while (clazz.getSuperclass() != null) {
            fields.addAll(Arrays.asList(clazz.getDeclaredFields()));
            clazz = clazz.getSuperclass();
        }
        return fields;
    }

    private <T extends Pojo> BoundStatement setValueToBoundStatement(
            int i, T pojo, Field field, BoundStatement statement) {
        Class<?> clazz = field.getType();
        try {
            if (field.getType() == UUID.class) {
                return statement.set(i, (UUID) field.get(pojo), UUID.class);
            } else if (field.getType().getTypeName().equals("int")) {
                return statement.setInt(i, (int) field.get(pojo));
            } else if ((field.getType() == String.class)) {
                return statement.setString(i, (String) field.get(pojo));
            }
        } catch (IllegalAccessException e) {
            new RuntimeException(e);
        }
        throw new RuntimeException(
                "Can't set value to the BoundStatement. No such setter for" + clazz.getName());
    }

    private PreparedStatement getPrepareStatement(String tableName, List<Field> fields) {
        RegularInsert regularInsert = QueryBuilder.insertInto(tableName)
                .value(fields.get(0).getName(), QueryBuilder.bindMarker());
        for (int i = 1; i < fields.size(); i++) {
            Field field = fields.get(i);
            regularInsert = regularInsert.value(field.getName(), QueryBuilder.bindMarker());
        }
        SimpleStatement insertStatement = regularInsert.build().setKeyspace(dbName);
        return session.prepare(insertStatement);
    }

        @Override
    public String getStoreWithMaxProductsByCategory(String category) {
        UUID categoryId = getCategoryId(category);
        logger.info("Starting searching query for \"{}\"", category);
        Map.Entry<UUID, Integer> totalByStoreId = getTotalByStoreId(categoryId);
        SimpleStatement findStore = QueryBuilder.selectFrom(dbName, "store")
                .columns("name",  "address")
                .whereColumn("id")
                .isEqualTo(literal(totalByStoreId.getKey()))
                .allowFiltering().build();
        ResultSet storeResultSet = session.execute(findStore);
        Row row = storeResultSet.one();
        if (row == null){
            return "No such records for " + category;
        }
        String storeName = row.getString("name");
        String storeAddress = row.getString("address");
        int totalProducts = totalByStoreId.getValue();
        return getMessage(category, totalProducts, storeName, storeAddress);
    }

    private Map.Entry<UUID, Integer> getTotalByStoreId(UUID categoryId) {
        String query = String.format("SELECT storeid, quantity AS total FROM test.stock " +
                "WHERE categoryid = %s;", categoryId.toString());
        stopWatch.restart();
        ResultSet resultSet = session.execute(query);
        Map<UUID, Integer> aggregationByStoreId = StreamSupport.stream(resultSet.spliterator(), true)
                .collect(Collectors.toMap(
                        row -> row.getUuid("storeid"),
                        row -> row.getInt("total"),
                        Integer::sum));
        logger.info("Search time is {} millis", stopWatch.getTime(TimeUnit.MILLISECONDS));
        return aggregationByStoreId.entrySet().stream()
                .max(Map.Entry.comparingByValue())
                .get();
    }

    private UUID getCategoryId(String category) {
        SimpleStatement simpleStatement = QueryBuilder.selectFrom(dbName, "category")
                .column("id")
                .whereColumn("name")
                .isEqualTo(bindMarker())
                .allowFiltering()
                .build().setKeyspace(dbName);
        PreparedStatement preparedStatement = session.prepare(simpleStatement);
        BoundStatement boundStatement = preparedStatement.bind(category);
        ResultSet resultSet = session.execute(boundStatement);
        return Objects.requireNonNull(resultSet.one()).get("id", UUID.class);
    }

    @Override
    public void dropDB(String name) {
        try {
            Drop drop = SchemaBuilder.dropKeyspace(name);
            session.execute(drop.build());
            logger.info("Keyspace \"{}\" was dropped!", name);
        } catch (InvalidQueryException e) {
            logger.warn("No such keyspace \"{}\" to drop!", name, e);
        }
    }

    @Override
    public void createIndexes(String tableName, List<String> fields) {
        logger.info("Creating indexes for {} fields in {} table", fields, tableName);
        for (String field : fields) {
            String query = String.format("CREATE INDEX %sidx ON test.stock (%s);", field, field);
            session.execute(query);
        }
    }

    public void close() {
        session.close();
    }

    public CassandraDB setDbName(String dbName) {
        this.dbName = dbName;
        return this;
    }
}
