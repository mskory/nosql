package com.mskory.util;

import com.mskory.models.Pojo;
import org.apache.commons.csv.CSVRecord;

import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.List;

public abstract class PojoMapper {

    public static  <T extends Pojo> List<T> map(List<CSVRecord> records, Class<T> clazz) {
        List<T> pojoList = new ArrayList<>(records.size());
        for (CSVRecord record : records) {
            T pojo;
            try {
                pojo = clazz.getDeclaredConstructor().newInstance();
            } catch (InstantiationException | IllegalAccessException | NoSuchMethodException |
                     InvocationTargetException e) {
                throw new RuntimeException(e);
            }
            for (Field field : pojo.getClass().getDeclaredFields()) {
                String fieldName = field.getName();
                if (record.get(fieldName) != null) {
                    field.setAccessible(true);
                    try {
                        field.set(pojo, record.get(fieldName));
                    } catch (IllegalAccessException e) {
                        throw new RuntimeException(e);
                    }
                    field.setAccessible(false);
                }
            }
            pojoList.add(pojo);
        }
        return pojoList;
    }
}
