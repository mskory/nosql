package com.mskory.models;

import com.mongodb.DBRef;
import jakarta.validation.constraints.Pattern;
import jakarta.validation.constraints.Size;
import org.bson.types.ObjectId;

import java.util.List;
import java.util.Objects;
import java.util.UUID;

public class Product extends Pojo{
    private String  name;
    private UUID categoryId;

    public Product() {
        this.setId(UUID.randomUUID());
    }

    public UUID getCategoryId() {
        return categoryId;
    }

    public Product setCategoryId(UUID categoryId) {
        this.categoryId = categoryId;
        return this;
    }

    @Pattern(regexp = "\\w*[aA]+\\w*")
    @Size(min = 5, max = 20, message = "{category.name.invalid}")
    public String getName() {
        return name;
    }

    public Product setName(String name) {
        this.name = name;
        return this;
    }
}
