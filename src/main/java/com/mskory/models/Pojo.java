package com.mskory.models;

import java.util.UUID;

public class Pojo {
    private UUID id;

    public UUID getId() {
        return id;
    }

    public Pojo setId(UUID id) {
        this.id = id;
        return this;
    }

    @Override
    public String toString() {
        return "Pojo{" +
                "id=" + id +
                '}';
    }
}
