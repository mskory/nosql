package com.mskory.dao;

import com.mskory.models.Pojo;
import com.mskory.util.Configuration;
import com.mskory.util.CsvReaderUtil;
import com.mskory.util.PojoMapper;
import com.mskory.util.StopWatch;
import org.apache.commons.csv.CSVRecord;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.List;

public interface DB {
    StopWatch stopWatch = new StopWatch();
    Configuration config = Configuration.getInstance();
    Logger logger = LoggerFactory.getLogger(DB.class);

    <T extends Pojo> void insertMany(String tableName, Class<T> clazz, List<T> dto);

    void createDB(String name);
    void dropDB(String name);

    void close();

    String getStoreWithMaxProductsByCategory(String category);

    void createIndexes(String tableName, List<String> fields);

    default  <T extends Pojo> List<T> insertFromCSV(String fileName, String[] headers, Class<T> clazz, String collectionName) {
        List<CSVRecord> storesRecords = CsvReaderUtil.read(fileName, headers);
        List<T> pojoList = PojoMapper.map(storesRecords, clazz);
        this.insertMany(collectionName, clazz, pojoList);
        return pojoList;
    }

    default String getMessage(String category, int totalProducts, String storeName, String storeAddress) {
        return String.format("(%s) %s %sTotal products in %s category: %s",
                storeName, storeAddress, System.lineSeparator(), category, totalProducts);
    }
}
