package com.mskory.util;

import java.util.concurrent.TimeUnit;

public class StopWatch {
    long startTime;

    public StopWatch() {
    }

    public double getTime(){
        return (System.currentTimeMillis() - startTime) / 1000d;
    }
    public double getTime(TimeUnit timeUnit){
        long workingTimeInMillis =  System.currentTimeMillis() - startTime;
        return timeUnit.convert(workingTimeInMillis, TimeUnit.MILLISECONDS);
    }

    public void restart (){
        this.startTime = System.currentTimeMillis();
    }

    public double getRPS(int numberOfOperations) {
        return numberOfOperations / getTime();
    }
}
