package com.mskory.util;

import org.apache.commons.lang3.RandomStringUtils;

import java.util.concurrent.ThreadLocalRandom;

public abstract class RandomUtil {

    public static String getString(int maxLength) {
        return RandomStringUtils.randomAlphabetic(ThreadLocalRandom.current()
                .nextInt(1, maxLength + 1));
    }

    public static int getInt(int max) {
        return ThreadLocalRandom.current().nextInt(0, max + 1);
    }
}
