package com.mskory;

import com.mskory.dao.CassandraDB;
import com.mskory.dao.DB;
import com.mskory.models.Category;
import com.mskory.models.Product;
import com.mskory.models.Stock;
import com.mskory.models.Store;
import com.mskory.util.Configuration;
import com.mskory.service.DtoGenerator;
import com.mskory.service.ProductDtoGenerator;
import com.mskory.util.RandomUtil;

import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

public class Main {
    public static final Configuration config = Configuration.getInstance();
    public static final String DB_NAME = "test";
    public static final String CATEGORY = "овочі";
    public static final String STORES_CSV_FILE_NAME = "stores.csv";
    public static final String CATEGORIES_CSV_FILE_NAME = "categories.csv";
    public static final String[] STORES_CSV_HEADERS = new String[]{"name", "address"};
    public static final String[] CATEGORIES_CSV_HEADERS = new String[]{"name"};

    public static void main(String[] args) {
        DB db = new CassandraDB();
        db.dropDB(DB_NAME);
        db.createDB(DB_NAME);

        List<Category> categories = db.insertFromCSV(CATEGORIES_CSV_FILE_NAME,
                CATEGORIES_CSV_HEADERS, Category.class, "category");
        List<Store> stores = db.insertFromCSV(STORES_CSV_FILE_NAME,
                STORES_CSV_HEADERS, Store.class, "store");
        DtoGenerator<Product> productDtoGenerator = new ProductDtoGenerator(categories);
        List<Product> products = productDtoGenerator.generateList();
        db.insertMany("product", Product.class, products);
        int maxQuantity = config.getMaxQuantityPerProduct();
        List<Stock> stockByStore = products.stream()
                .map(p -> new Stock()
                        .setProductId(p.getId()).setCategoryId(p.getCategoryId()))
                .collect(Collectors.toList());
        for (Store store : stores) {
            stockByStore.forEach(d -> d
                    .setStoreId(store.getId())
                    .setQuantity(RandomUtil.getInt(maxQuantity))
                    .setId(UUID.randomUUID()));
            db.insertMany("stock", Stock.class, stockByStore);
        }

//        db.createIndexes("stock", List.of("categoryId"));

        System.out.println(db.getStoreWithMaxProductsByCategory(CATEGORY));
        db.close();


    }
}