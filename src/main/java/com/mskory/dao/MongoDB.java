package com.mskory.dao;

import com.google.gson.Gson;
import com.mongodb.*;
import com.mongodb.client.*;
import com.mongodb.client.model.Indexes;
import com.mskory.models.Pojo;
import com.mskory.util.StopWatch;
import org.bson.Document;
import org.bson.UuidRepresentation;
import org.bson.codecs.configuration.CodecProvider;
import org.bson.codecs.configuration.CodecRegistry;
import org.bson.codecs.pojo.PojoCodecProvider;
import org.bson.conversions.Bson;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;
import java.util.concurrent.TimeUnit;

import static com.mongodb.MongoClientSettings.getDefaultCodecRegistry;
import static com.mongodb.client.model.Accumulators.sum;
import static com.mongodb.client.model.Aggregates.*;
import static com.mongodb.client.model.Filters.eq;
import static com.mongodb.client.model.Sorts.descending;
import static org.bson.codecs.configuration.CodecRegistries.fromProviders;
import static org.bson.codecs.configuration.CodecRegistries.fromRegistries;

public class MongoDB implements DB {
    Logger logger = LoggerFactory.getLogger(this.getClass());
    CodecRegistry pojoCodecRegistry;
    MongoClientSettings settings;
    MongoClient mongoClient;
    MongoDatabase database;
    int batchSize;

    public MongoDB() {
        ConnectionString connectionString = new ConnectionString(config.getMongoURL());
        batchSize = config.getBatchSize();
        ServerApi serverApi = ServerApi.builder()
                .version(ServerApiVersion.V1)
                .build();
        settings = MongoClientSettings.builder()
                .applyConnectionString(connectionString)
                .uuidRepresentation(UuidRepresentation.STANDARD)
                .serverApi(serverApi)
                .build();
        CodecProvider pojoCodecProvider = PojoCodecProvider.builder().automatic(true).build();
        pojoCodecRegistry = fromRegistries(getDefaultCodecRegistry(), fromProviders(pojoCodecProvider));
        mongoClient = MongoClients.create(settings);
    }

    @Override
    public <T extends Pojo> void insertMany(String tableName, Class<T> clazz, List<T> dto) {
        stopWatch.restart();
        MongoCollection<T> categoryMongoCollection = database.getCollection(tableName, clazz);
//        dto.forEach(categoryMongoCollection::insertOne);
        if (dto.size() > batchSize) {
            List<T> batch = new ArrayList<>(batchSize);
            for (T pojo : dto) {
                batch.add(pojo);
                if (batch.size() == batchSize) {
                    categoryMongoCollection.insertMany(batch);
                    batch.clear();
                }
            }
        }
        categoryMongoCollection.insertMany(dto);
        logger.info("{} records were added to {} in {} seconds", dto.size(), tableName, stopWatch.getTime());
        logger.info("RPS is {}", stopWatch.getRPS(dto.size()));
    }

    public <T extends Pojo> void insertMany(String collectionName, List<T> dto) {
        stopWatch.restart();
        Gson gson = new Gson();
        MongoCollection<Document> categoryMongoCollection = database.getCollection(collectionName);
        List<Document> batch = new ArrayList<>(batchSize);
        for (T pojo : dto) {
            batch.add(Document.parse(gson.toJson(pojo)));
            if (batch.size() == batchSize) {
                categoryMongoCollection.insertMany(batch);
                batch.clear();
            }
            categoryMongoCollection.insertMany(batch);
        }
        logger.info("{} records were added to {}", dto.size(), collectionName);
        logger.info("RPS is {}", stopWatch.getRPS(dto.size()));
    }

    @Override
    public String getStoreWithMaxProductsByCategory(String category) {
        UUID categoryId = getCategoryId(category);
        logger.info("Starting searching query for \"{}\"", category);
        stopWatch.restart();
        Document totalWithStoreId = getTotalWithStoreId(categoryId);
        logger.info("Search time is {} millis", stopWatch.getTime(TimeUnit.MILLISECONDS));
        UUID storeId = totalWithStoreId.get("_id", UUID.class);
        Document storeInfo = getStoreInfo(storeId);
        String storeName = storeInfo.getString("name");
        String storeAddress = storeInfo.getString("address");
        int totalProducts = totalWithStoreId.getInteger("total");
        return getMessage(category, totalProducts, storeName, storeAddress);
    }

    private Document getStoreInfo(UUID id) {
        MongoCollection<Document> stores = database.getCollection("store");
        return stores.find(eq("_id", id)).first();
    }

    private Document getTotalWithStoreId(UUID categoryId) {
        MongoCollection<Document> searchCollection = database.getCollection("stock");
        Bson match = match(eq("categoryId", categoryId));
        Bson group = group("$stock.storeId",
                sum("total", "$stock.quantity"));
        Bson sort = sort(descending("total"));
        Bson limit = limit(1);
        Document result = searchCollection.aggregate(
                        List.of(match, group, sort, limit))
                .first();
        return result;
    }

    private UUID getCategoryId(String category) {
        MongoCollection<Document> categories = database.getCollection("category");
        FindIterable<Document> documents = categories.find(eq("name", category));
        Document document = documents.first();
        assert document != null;
        return document.get("_id", UUID.class);
    }

    @Override
    public void createDB(String name) {
        try {
            database = mongoClient.getDatabase(name).withCodecRegistry(pojoCodecRegistry);
            database.runCommand(new Document("ping", 1));
            logger.info("Pinged deployment. Successfully connected to MongoDB!");
        } catch (MongoException e) {
            throw new RuntimeException("Unable to connect to the MongoDB instance" + e);
        }
    }

    @Override
    public void createIndexes(String collectionName, List<String> fields) {
        logger.info("Creating indexes for {} document fields in {} collection", fields, collectionName);
        MongoCollection<Document> collection = database.getCollection(collectionName);
        fields.forEach(f -> collection.createIndex(Indexes.ascending(f)));
    }

    @Override
    public void dropDB(String dbName) {
        if (isDbExists(dbName)) {
            mongoClient.getDatabase(dbName).drop();
            logger.info("Database \"{}\" was dropped!", dbName);
            return;
        }
        logger.info("No such database \"{}\" to drop!", dbName);
    }

    @Override
    public void close() {
        mongoClient.close();
    }

    private boolean isDbExists(String dbName) {
        for (String db : mongoClient.listDatabaseNames()) {
            if (db.equals(dbName)) {
                return true;
            }
        }
        return false;
    }
}
