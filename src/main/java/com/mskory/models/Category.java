package com.mskory.models;

import java.io.Serializable;
import java.util.UUID;

public class Category extends Pojo implements Serializable {

    private String name;

    public Category() {
        this.setId(UUID.randomUUID());
    }

    public String getName() {
        return name;
    }

    public Category setName(String name) {
        this.name = name;
        return this;
    }

    @Override
    public String toString() {
        return "Category{" +
                "name='" + name + '\'' +
                "} " + super.toString();
    }
}
