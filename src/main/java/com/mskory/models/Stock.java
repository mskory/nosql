package com.mskory.models;

import java.util.UUID;

public class Stock extends Pojo{
    UUID productId;
    UUID categoryId;
    UUID storeId;
    int quantity;

    public Stock() {
        this.setId(UUID.randomUUID());
    }

    public UUID getProductId() {
        return productId;
    }

    public Stock setProductId(UUID productId) {
        this.productId = productId;
        return this;
    }

    public UUID getStoreId() {
        return storeId;
    }

    public Stock setStoreId(UUID storeId) {
        this.storeId = storeId;
        return this;
    }

    public int getQuantity() {
        return quantity;
    }

    public Stock setQuantity(int quantity) {
        this.quantity = quantity;
        return this;
    }

    public UUID getCategoryId() {
        return categoryId;
    }

    public Stock setCategoryId(UUID categoryId) {
        this.categoryId = categoryId;
        return this;
    }
}
