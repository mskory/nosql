package com.mskory.service;

import com.mskory.models.Category;
import com.mskory.models.Product;
import com.mskory.util.RandomUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class ProductDtoGenerator implements DtoGenerator<Product> {
    private static final int MAX_NAME_LENGTH = 30;
    private final Logger logger = LoggerFactory.getLogger(ProductDtoGenerator.class);
    private final List<Category> categories;

    public ProductDtoGenerator(List<Category> categories) {
        this.categories = categories;
    }

    @Override
    public List<Product> generateList() {
        List<Product> products = Stream.generate(() -> new Product()
                        .setName(RandomUtil.getString(MAX_NAME_LENGTH))
                        .setCategoryId(getRandomCategoryId()))
                .parallel()
                .filter(product -> validator.validate(product).isEmpty())
                .limit(config.getProductsAmount())
                .collect(Collectors.toList());
        logger.info("{} products were generated", config.getProductsAmount());
        return products;
    }

    private UUID getRandomCategoryId() {
        return categories.get(RandomUtil.getInt(categories.size() - 1)).getId();
    }
}
