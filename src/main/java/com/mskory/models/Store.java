package com.mskory.models;

import java.util.UUID;

public class Store extends Pojo{
    private String name;
    private String address;

    public Store() {
        this.setId(UUID.randomUUID());
    }

    public String getName() {
        return name;
    }

    public Store setName(String name) {
        this.name = name;
        return this;
    }

    public String getAddress() {
        return address;
    }

    public Store setAddress(String address) {
        this.address = address;
        return this;
    }

    @Override
    public String toString() {
        return "Store{" +
                "name='" + name + '\'' +
                ", address='" + address + '\'' +
                "} " + super.toString();
    }
}
