package com.mskory.service;

import com.mskory.util.Configuration;
import jakarta.validation.Validation;
import jakarta.validation.Validator;

import java.util.List;

public interface DtoGenerator <T> {
    Configuration config =  Configuration.getInstance();
    Validator validator = Validation.buildDefaultValidatorFactory().getValidator();
    List<T> generateList();
}
