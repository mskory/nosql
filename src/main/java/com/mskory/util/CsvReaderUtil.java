package com.mskory.util;

import com.mskory.models.Pojo;
import com.mskory.models.Store;
import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVRecord;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.io.InputStreamReader;
import java.io.Reader;
import java.util.List;
import java.util.stream.Collectors;

public abstract class CsvReaderUtil {
    private static final Logger logger  = LoggerFactory.getLogger(CsvReaderUtil.class);

    public static List<CSVRecord> read(String filename, String[] headers) {
        CSVFormat format;
        if (headers == null || headers.length == 0){
            format = CSVFormat.RFC4180.builder().build();
        }else {
            format = CSVFormat.RFC4180.builder()
                    .setHeader(headers)
                    .setSkipHeaderRecord(true)
                    .build();
        }
        try (Reader reader = new InputStreamReader(PropertiesReaderUtil.getResources(filename))){
            List<CSVRecord> records = format.parse(reader).getRecords();
            printLogs(records.size(), filename);
            return records;
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    public static List<CSVRecord> read(String filename) {
        return read(filename, null);
    }

    private static void printLogs(int numberOfRecords, String filename){
        logger.info("{} records from {} file were read", numberOfRecords, filename);
    }
}
