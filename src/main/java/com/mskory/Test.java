package com.mskory;

import com.mskory.dao.CassandraDB;
import com.mskory.dao.DB;
import com.mskory.dao.MongoDB;
import com.mskory.models.Category;
import com.mskory.models.Stock;
import com.mskory.models.Store;

import java.util.List;
import java.util.UUID;
import java.util.stream.Stream;

public class Test {
    private static final String TABLE_NAME = "stock";
    private static final String DB_NAME = "test";

    public static final String STORES_CSV_FILE_NAME = "stores.csv";
    public static final String CATEGORIES_CSV_FILE_NAME = "categories.csv";
    public static final String[] STORES_CSV_HEADERS = new String[]{"name", "address"};
    public static final String[] CATEGORIES_CSV_HEADERS = new String[]{"name"};

    public static void main(String[] args) {
        DB db = new MongoDB();
        db.dropDB("test");
        db.createDB("test");
        List<Category> categories = db.insertFromCSV(CATEGORIES_CSV_FILE_NAME,
                CATEGORIES_CSV_HEADERS, Category.class, "category");
//        db.createIndexes("stock", List.of("categoryId"));

//        System.out.println(db.getStoreWithMaxProductsByCategory("овочі"));
        db.close();
//        Stock stock = new Stock().setStoreId(UUID.randomUUID()).setQuantity(1).setCategoryId(UUID.randomUUID());
//        Stock stock0 = new Stock().setStoreId(stock.getStoreId()).setQuantity(2);
//        Stock stock1 = new Stock().setStoreId(UUID.randomUUID()).setQuantity(3);
//        Stock stock2 = new Stock().setStoreId(stock1.getStoreId()).setQuantity(4);
//        List<Stock> stocks = List.of(stock0, stock, stock1, stock2) ;
//        db.insertMany("stock", Stock.class, stocks);
//        db.close();

    }
}





