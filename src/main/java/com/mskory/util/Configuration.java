package com.mskory.util;

import java.util.Properties;

public class Configuration {
    public static final String PROPERTIES_FILE_NAME = "app.properties";
    private static Configuration config;
    String hostname;
    int cassandraPort;
    private String user;
    private String password;
    private String mongoURL;
    private String datacenter;
    private int productsAmount;
    private int quantityPerProduct;
    private int poolSize;
    private int batchSize;

    private Configuration() {
    }

    public static Configuration getInstance() {
        if (config == null) {
            Properties properties = PropertiesReaderUtil.getProperties(PROPERTIES_FILE_NAME);
            String url = properties.getProperty("mongoUrl");
            String mongoURL;
            if (url.contains("@mongodb")) {
                String[] splitUrl = url.split("@mongodb");
                mongoURL = splitUrl[0] + properties.getProperty("user") + ":"
                        + properties.getProperty("password") + "@mongodb" + splitUrl[1];
            } else {
                mongoURL = url;
            }
            config = new Configuration()
                    .setHostname(properties.getProperty("hostname"))
                    .setCassandraPort(Integer.parseInt(properties.getProperty("cassandraPort")))
                    .setUser(properties.getProperty("user"))
                    .setPassword(properties.getProperty("password"))
                    .setMongoURL(mongoURL)
                    .setProductsAmount(Integer.parseInt(properties.getProperty("productsAmount")))
                    .setMaxQuantityPerProduct(Integer.parseInt(properties.getProperty("maxQuantityPerProduct")))
                    .setPoolSize(Integer.parseInt(properties.getProperty("poolSize")))
                    .setBatchSize(Integer.parseInt(properties.getProperty("batchSize")))
                    .setDatacenter(properties.getProperty("cassandraDC"));
        }
        return config;
    }

    public String getDatacenter() {
        return datacenter;
    }

    public Configuration setDatacenter(String datacenter) {
        this.datacenter = datacenter;
        return this;
    }

    @Override
    public String toString() {
        return "Configuration{" +
                "user='" + user + '\'' +
                ", password='" + password + '\'' +
                ", mongoURL='" + mongoURL + '\'' +
                ", productsAmount=" + productsAmount +
                ", quantityPerProduct=" + quantityPerProduct +
                ", poolSize=" + poolSize +
                '}';
    }

    public String getHostname() {
        return hostname;
    }

    public Configuration setHostname(String hostname) {
        this.hostname = hostname;
        return this;
    }

    public int getCassandraPort() {
        return cassandraPort;
    }

    public Configuration setCassandraPort(int cassandraPort) {
        this.cassandraPort = cassandraPort;
        return this;
    }

    public String getUser() {
        return user;
    }

    public Configuration setUser(String user) {
        this.user = user;
        return this;
    }

    public String getPassword() {
        return password;
    }

    public Configuration setPassword(String password) {
        this.password = password;
        return this;
    }

    public String getMongoURL() {
        return mongoURL;
    }

    public Configuration setMongoURL(String mongoURL) {
        this.mongoURL = mongoURL;
        return this;
    }

    public int getProductsAmount() {
        return productsAmount;
    }

    public Configuration setProductsAmount(int productsAmount) {
        this.productsAmount = productsAmount;
        return this;
    }

    public int getMaxQuantityPerProduct() {
        return quantityPerProduct;
    }

    public Configuration setMaxQuantityPerProduct(int quantityPerProduct) {
        this.quantityPerProduct = quantityPerProduct;
        return this;
    }

    public int getPoolSize() {
        return poolSize;
    }

    public Configuration setPoolSize(int poolSize) {
        this.poolSize = poolSize;
        return this;
    }

    public int getBatchSize() {
        return batchSize;
    }

    public Configuration setBatchSize(int batchSize) {
        this.batchSize = batchSize;
        return this;
    }
}
